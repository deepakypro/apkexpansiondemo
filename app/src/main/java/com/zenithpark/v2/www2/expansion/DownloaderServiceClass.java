package com.zenithpark.v2.www2.expansion;

/**
 * Created by deepakyadav on 17/04/18.
 */

import com.google.android.vending.expansion.downloader.impl.DownloaderService;


public class DownloaderServiceClass extends DownloaderService {

  // stuff for LVL -- MODIFY FOR YOUR APPLICATION!
  private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlD+Ii9DTXlG4vsfAOWrw++qs87pzOf2fQl9erF+DBojN2gKTL2+VxHaBJWWa9PTffKdtPLv0CGmp+eG6fhdV4MaTnNkW71AWoBQSHr8eZDL/uoTM/I1qku1vbyjsg2aKUP0bXH0gf465iUzSu6WALNooRYetLzSkgRCZDIuFYUKLH2/VxHEe1B/QCyx6X3nNITQoa/XLmtuF/WLeS7DLFrcgKYohh9fpywLXi9eQhgjVA4941MWgkB9D95adrLVMAA1+D68nOUMg0V7xSuFrM51Jn+dzjpcpdN0I7AiEordHZEqdzrAIwiz1T+/20+mLsyRCbB6ycavGdk/f6W2i3wIDAQAB";
  // used by the preference obfuscater
  public static final byte[] SALT = new byte[] { 1, 42, -12, -1, 54, 98,
      -100, -12, 43, 2, -8, -4, 9, 5, -106, -107, -33, 45, -1, 84
  };


  /**
   * This public key comes from your Android Market publisher account, and it
   * used by the LVL to validate responses from Market on your behalf.
   */
  @Override
  public String getPublicKey() {
    return BASE64_PUBLIC_KEY;
  }

  /**
   * This is used by the preference obfuscater to make sure that your
   * obfuscated preferences are different than the ones used by other
   * applications.
   */
  @Override
  public byte[] getSALT() {
    return SALT;
  }

  /**
   * Fill this in with the class name for your alarm receiver. We do this
   * because receivers must be unique across all of Android (it's a good idea
   * to make sure that your receiver is in your unique package)
   */
  @Override
  public String getAlarmReceiverClassName() {
    return DownloaderServiceBroadcastReceiver.class.getName();
  }

}
