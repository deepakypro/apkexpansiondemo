package com.zenithpark.v2.www2.expansion;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;

/**
 * Created by deepakyadav on 17/04/18.
 */

public class DownloaderServiceBroadcastReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(Context context, Intent intent) {
    try {
      DownloaderClientMarshaller.startDownloadServiceIfRequired(context, intent, DownloaderServiceClass.class);
    } catch (NameNotFoundException e) {
      e.printStackTrace();
    }
  }

}